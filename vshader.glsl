# version 130

in vec4 vPosition;
in vec3 vertexColor;

out vec3 fragmentColor;

// Values that stay constant for the whole mesh
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform int pointk;

// surface attributes, and uniform variables
in mat4 xy_s_Matrix;
uniform mat4 z_s_rotate;
uniform mat4 move;
uniform mat4 translate;
uniform mat4 cube_rotate;
uniform float w,h;

// here is the Motaz Part for texture.
out vec2 texCoord; 

out vec4 n;
out mat4 transformation;
out vec4 position;

void main()
{
	if(pointk == 1){
    	// surface vertex so apply the transformation
    	gl_Position = projection * view * move * translate * xy_s_Matrix * z_s_rotate * model * vPosition;      
    	
    	// here is the Motaz Part for texture.
	    texCoord.s = ((vPosition.x+w)/(2*w)); 
	    texCoord.t = ((vPosition.z+h)/(2*h));
    }else{
    	// normal point
    	gl_Position = projection * view * move * translate * xy_s_Matrix * z_s_rotate * model * vPosition;
    }
    
    // The color of each vertex will be interpolated
	// to produce the color of each fragment
	fragmentColor = vertexColor;
	
	// lighting and shading part
    transformation = move * translate * xy_s_Matrix * z_s_rotate * model;
	
	// n = normal vector to point ( in y direction then apply transformation )	
	// normal vector -- to be checked
	n = normalize(transformation * vec4(vPosition.x, vPosition.y + 1.0, vPosition.z, 1.0));
	
	position = vPosition; 
}

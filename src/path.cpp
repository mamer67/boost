/*
 * path.cpp
 *
 *  Created on: Dec 15, 2013
 *      Author: Aamer
 */

#include "path.h"
#include "iostream"

using namespace std;

int number = 0;
int fannub = 0;
int diff = 8;

static mat4 mp[fan];

// all surface functions
// ******************************************************************

/**
 * ax is either, -1 is down "clockwise", 1 is up "anti clockwise", 0 no rotation
 * ay is either, -1 is left, "anti clockwise", 1 is right "clockwise", 0 no rotation
 */
surface::surface(bool bcube, GLfloat ax, GLfloat ay, GLfloat anglez, int place, int fanno, GLuint program, GLfloat r, GLfloat g, GLfloat b) {

	/**
	 * The value of width is calculated to generate 12 surface fan
	 * each rotated 30 degrees, so don't change unless u know what
	 * you do
	 */

	width = 2 * height * tan(DegreesToRadians * (360.0 / (2 * fan)));
//	depth = width;

	this->bcube = bcube;
	this->orientation = RotateZ(anglez);
	this->place = place;
	this->program = program;
	this->ax = ax;
	this->ay = ay;

	m1 = mp[fanno];

//	mat4 m3 = transpose(RotateY(place - 1 > 0 ? ay * roty * (place - 1) : 0) * RotateX(place - 1 > 0 ? ax * rotx * (place - 1) : 0));
//	mat4 m2 = transpose(RotateY(ay * roty * place) * RotateX(ax * rotx * place));
	m2 = m1 * transpose(RotateY(this->ax * roty) * RotateX(this->ay * rotx));

//	cout << "m1" << endl;
//	printm(m1);
//	cout << "m3" << endl;
//	printm(m3);

	GLfloat _pts[] = {
	//	surface points
			-width / 2, -1 * height, -0.5f * depth + depth * place, r, g, b, //0
			width / 2, -1 * height, -0.5f * depth + depth * place, r, g, b, //1

			width / 2, -1 * height, depth * (place + 0.5f), r, g, b, //2
			-width / 2, -1 * height, depth * (place + 0.5f), r, g, b //3
			};

	GLuint _vorder[] = {
	//	surface_vertex order
			0, 1, 2 //
			, 2, 3, 0 //
			};

	// 3 points for x,y,z
	// 3 points for r,g,b
	// 16 points for 4*4 transformation matrix
	for (int i = 0; i < 4; ++i) {
		// copy first 6 points
		for (int j = 0; j < 6; ++j) {
			_points[i * 22 + j] = _pts[i * 6 + j];
		}

		// copy 4*4 matrix m1
		for (int k = 0; k < 4; ++k) {
			for (int l = 0; l < 4; ++l) {
				if (i < 2)
					_points[i * 22 + 6 + (k * 4 + l)] = m1[k][l];
				else
					_points[i * 22 + 6 + (k * 4 + l)] = m2[k][l];
			}
		}
	}

	mp[fanno] = m2;

//	cout << "m2" << endl;
//	printm(m2);
//	cout << "m fanno" << endl;
//	printm(mp[fanno]);

	// Magdy Part **********************************************************/
	if (bcube) {
		this->mycube.createCube(program, 0, -1 * height + .25, depth * place, 0, 0, 1, m1, m2);
	}
//	number++;
	// *********************************************************************/

	// create vao
	// create a vertex array object
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// create and initialize a vertex buffer object
	glGenBuffers(1, &vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(_points), _points, GL_STATIC_DRAW);

	// create vertex_order_buffer to save the order of the vertices
	GLuint vertex_order_buffer;
	glGenBuffers(1, &vertex_order_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertex_order_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(_vorder), _vorder,
	GL_STATIC_DRAW);

	// get vertex and color attributes
	GLuint slocation = glGetAttribLocation(program, "vPosition");
	GLuint obj_color = glGetAttribLocation(program, "vertexColor");
	GLuint pos = glGetAttribLocation(program, "xy_s_Matrix");

	// get uniform rotation for the model
	rotation = glGetUniformLocation(program, "z_s_rotate");
	ptype = glGetUniformLocation(program, "pointk");
	ww = glGetUniformLocation(program, "w");
	hh = glGetUniformLocation(program, "h");

	// bind the slocation attribute with the vertices of the buffer
	glEnableVertexAttribArray(slocation);
	glVertexAttribPointer(slocation, 3, GL_FLOAT, GL_FALSE, 22 * sizeof(GLfloat), BUFFER_OFFSET(0));

	// bind the obj_color attribute to the colors attached to the vertex
	glEnableVertexAttribArray(obj_color);
	glVertexAttribPointer(obj_color, 3, GL_FLOAT, GL_FALSE, 22 * sizeof(GLfloat), BUFFER_OFFSET(3*sizeof(GLfloat)));

	// bind the transformation matrix
	GLuint pos1 = pos + 0;
	GLuint pos2 = pos + 1;
	GLuint pos3 = pos + 2;
	GLuint pos4 = pos + 3;
	glEnableVertexAttribArray(pos1);
	glEnableVertexAttribArray(pos2);
	glEnableVertexAttribArray(pos3);
	glEnableVertexAttribArray(pos4);

	glVertexAttribPointer(pos1, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 22, (void*) (sizeof(float) * 6));
	glVertexAttribPointer(pos2, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 22, (void*) (sizeof(float) * 10));
	glVertexAttribPointer(pos3, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 22, (void*) (sizeof(float) * 14));
	glVertexAttribPointer(pos4, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 22, (void*) (sizeof(float) * 18));
}

void surface::dec_place() {
	if (place <= 0)
		return;

	place = place - 1;

	GLfloat p1 = -0.5f * depth + depth * place;
	GLfloat p2 = depth * (place + 0.5f);

	mat4 tmp = m1;
	mat4 m1 = m2 * transpose(RotateY(-1 * this->ax * roty) * RotateX(-1 * this->ay * rotx));
	mat4 m2 = tmp;

	// 3 points for x,y,z
	// 3 points for r,g,b
	// 16 points for 4*4 transformation matrix
	for (int i = 0; i < 4; ++i) {
		// copy 4*4 matrix m1
		for (int k = 0; k < 4; ++k) {
			for (int l = 0; l < 4; ++l) {
				if (i < 2)
					_points[i * 22 + 6 + (k * 4 + l)] = m1[k][l];
				else
					_points[i * 22 + 6 + (k * 4 + l)] = m2[k][l];
			}
		}
	}

	_points[0 * 22 + 2] = p1;
	_points[1 * 22 + 2] = p1;
	_points[2 * 22 + 2] = p2;
	_points[3 * 22 + 2] = p2;

	// bind vao
	glBindVertexArray(vao);

	// create and initialize a vertex buffer object
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(_points), _points, GL_STATIC_DRAW);
}

void surface::draw() {
	glBindVertexArray(vao);

	glUniformMatrix4fv(rotation, 1, GL_TRUE, orientation);
	glUniform1i(ptype, SURFACE_P);
	glUniform1f(hh, depth / 2.0);
	glUniform1f(ww, width / 2.0);
	glDrawElements(GL_TRIANGLES, 2 * 3, GL_UNSIGNED_INT,
	BUFFER_OFFSET(0*sizeof(GLuint)));

	if (bcube) {
		glUniform1i(ptype, NORMAL_P);
		this->mycube.draw_cube();
	}
}

bool surface::has_cube() {
	return bcube;
}

cube surface::getCube() {
	return this->mycube;
}

mat4 surface::get_orientation() {
	return orientation;
}
// end of surface functions
// ******************************************************************

// all ring functions
// ******************************************************************

ring::ring(GLfloat anglex, GLfloat angley, int place, GLuint program, GLfloat r, GLfloat g, GLfloat b) {
	// start creating your objects here
	this->place = place;
//	cout << "here ring " << place << endl;

	surfcs = new surface*[fan];
	bool fv = false;
	bool hap = false;
	for (int i = 0; i < fan; ++i) {
		fv = place > 1 && i == fannub && number % diff == 0 ? true : false;
		if (fv)
			hap = true;
		surfcs[i] = new surface(fv, anglex, angley, i * 360.0 / fan, this->place, i, program, r, g, b);
	}

	if (hap) {
		fannub = (fannub + 1) % fan;
	}
	number++;
}

void ring::dec_place() {
	for (int i = 0; i < fan; ++i) {
		surfcs[i]->dec_place();
	}
}

void ring::draw() {
	for (int i = 0; i < fan; ++i) {
		surfcs[i]->draw();
	}
}

surface ** ring::getSur() {
	return this->surfcs;
}
// end of ring functions
// ******************************************************************


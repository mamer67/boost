/*
 * cube.h
 *
 *  Created on: Nov 23, 2013
 *      Author: AHMAD
 */
#include "include/Angel.h"
#include "const.h"
#ifndef CUBE_H_
#define CUBE_H_

class cube {
public:
	vec4 points[40];
	vec3 colors[8];
	mat4 transformation;
	GLuint trans_loc;
	GLint row, column;
	GLfloat centerX, centerY, centerZ;
	GLint mynumber;
	cube();
	void createCube(GLuint program, GLfloat centerx, GLfloat centery,
			GLfloat centerz, GLint r, GLint c, GLint n, mat4 m1, mat4 m2);
	void draw_cube();
	virtual ~cube();

private:
	GLuint vao;
	GLuint vboID[2];
	GLuint ibo[1];
	void create_cube(GLfloat centerx, GLfloat centery, GLfloat centerz, mat4 m1, mat4 m2);
};

#endif /* CUBE_H_ */

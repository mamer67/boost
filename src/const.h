/*
 * const.h
 *
 *  Created on: Dec 19, 2013
 *      Author: aamer
 */

#ifndef CONST_H_
#define CONST_H_

/**
 * used to draw any normal point in the scene, this should be passed
 * to as uniform attribute ptype
 */
#define NORMAL_P 	0

/**
 * used to draw any normal point in the scene, this should be passed
 * to as uniform attribute ptype
 */
#define SURFACE_P 	1

/**
 * used as the space used by the user in 2D space of the tunnel
 * */
#define USER_DIM  0.5
/**
 * cube side
 *
 * */
static const GLfloat height = 2;
static const int fan = 20;
static const GLfloat CUBE_SIDE = 2 * height * tan(DegreesToRadians * (360.0 / (2 * fan)));
static const GLfloat depth = 2 * height * tan(DegreesToRadians * (360.0 / (2 * fan)));

#endif /* CONST_H_ */

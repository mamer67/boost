//============================================================================
// Name        : rubix.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "include/Angel.h"
#include "path.h"

#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;

// Lighting PART --------------------------------------------
// pointers to location of ambient, diffuse, specular,.. in v-shader
GLuint reflect_ambient_ptr, reflect_diffuse_ptr, light_loc_ptr, reflect_specular_ptr, shininess_ptr, light_ambient_ptr, light_diffuse_ptr, light_specular_ptr;
// vectors of components
vec4 light_ambient;
vec4 light_diffuse;
vec4 light_specular;

vec4 reflect_diffuse;
vec4 reflect_specular;

// light source position
vec4 light_pos;
GLfloat shininess;

// CPP PART----------------------------------------------------
// window dimension
GLfloat window_width = 800;
GLfloat window_height = 500;
const GLfloat _MOVE_S = 5;

GLint m, v, p, mv;
mat4 move;
mat4 projection, view, model;
vec4 camera;

unsigned char * textureData;
GLuint textWidth;
GLuint textHeight;
GLuint program;
bool stop = false;
bool dont = false;

static int II = 0;
int _BUFFER_SIZE = 200;
ring **rng;
int delay = 100;
GLfloat ts = 0.04;
GLfloat acs;

int start = 0;
GLuint rm;
mat4 translationm;

void generate_next(int);
bool check_collision();

//----------------------------------------------------------------------------
// This function for loading the texture image in memory.
void loadImageAndIniateShader(const char *imageName) {
	// Data read from the header of the BMP file
	unsigned char header[54]; // Each BMP file begins by a 54-bytes header
	unsigned int dataPos;   // Position in the file where the actual data begins
	unsigned int imageSize;   // = width*height*3
	// Actual RGB data
	FILE * file = fopen(imageName, "rb");
	if (!file) {
		printf("Image could not be opened\n");
	}
	if (fread(header, 1, 54, file) != 54) { // If not 54 bytes read : problem
		printf("Not a correct BMP file\n");
	}
	if (header[0] != 'B' || header[1] != 'M') {
		printf("Not a correct BMP file\n");
	}

	// Read ints from the byte array
	dataPos = *(int*) &(header[0x0A]);
	imageSize = *(int*) &(header[0x22]);
	textWidth = *(int*) &(header[0x12]);
	textHeight = *(int*) &(header[0x16]);
	// Some BMP files are misformatted, guess missing information
	if (imageSize == 0)
		imageSize = textWidth * textHeight * 3; // 3 : one byte for each Red, Green and Blue component
	if (dataPos == 0)
		dataPos = 54; // The BMP header is done that way
	// Create a buffer
	textureData = new unsigned char[imageSize];

	// Read the actual data from the file into the buffer
	fread(textureData, 1, imageSize, file);
	cout << "text width: " << textWidth << endl;
	cout << "text height: " << textHeight << endl;

	cout << "image has been loaded successfully " << endl;
	//Everything is in memory now, the file can be closed
	fclose(file);
}

// Display function ---------------------------------------------------------
void init(void) {

	// load texture image;
	loadImageAndIniateShader("surface.bmp");
	// generate a texture buffer.
	// Create one OpenGL texture
	GLuint textureID;

	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &textureID);


	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Give the image to OpenGLck
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textWidth, textHeight, 0, GL_BGR,
	GL_UNSIGNED_BYTE, textureData);

	// TODO Please revise this functionality.
	glGenerateMipmap(GL_TEXTURE_2D);
	//	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	//	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	// load shaders and use the resulting shader program
	program = InitShader("vshader.glsl", "fshader.glsl");
	glUseProgram(program);

	// initialize the vertex position attribute from the vertex shader
	m = glGetUniformLocation(program, "model");
	v = glGetUniformLocation(program, "view");
	p = glGetUniformLocation(program, "projection");
	mv = glGetUniformLocation(program, "move");
	rm = glGetUniformLocation(program, "translate");

	// projection matrix : 45° Field of View, 4:3 ratio, display range :0.1 unit <-> 100 units
	// review Amin_view_transfo to know about perspective matrix
	projection = Perspective(45.0f, window_width / window_height, 0.1f, 100.0f);

	/**
	 * we position the camera at 3,3,3 try changing this and see what happens
	 * we keep looking at the origin, COI, center of interest, as all our drawing in origin, w.r.t origin
	 * we look up,
	 */
	camera = vec4(0, -(height - 0.5), -1, 1);
	view = LookAt(camera, // Camera is at (4,3,-3), in World Space
			vec4(0, 0, 30, 1),  // and looks at the origin
			vec4(0, 1, 0, 1) // Head is up (set to 0,-1,0 to look upside-down)
					);

	// Model matrix : an identity matrix (model will be at the origin)

	// Mohamed PART Shading------------------------------------------
	GLuint eye_ptr = glGetUniformLocation(program, "eye");
	glUniform4f(eye_ptr, camera[0], camera[1], camera[2], camera[3]);

	light_ambient_ptr = glGetUniformLocation(program, "light_ambient");
	light_diffuse_ptr = glGetUniformLocation(program, "light_diffuse");
	light_specular_ptr = glGetUniformLocation(program, "light_specular");

	reflect_diffuse_ptr = glGetUniformLocation(program, "reflect_diffuse");
	reflect_specular_ptr = glGetUniformLocation(program, "reflect_specular");

	light_loc_ptr = glGetUniformLocation(program, "light_source_pos");

	light_pos = vec4(0, (height), 5, 1);
	glUniform4f(light_loc_ptr, light_pos[0], light_pos[1], light_pos[2], light_pos[3]);

	shininess_ptr = glGetUniformLocation(program, "shininess");
	shininess = 30.0;
	glUniform1f(shininess_ptr, shininess);
	light_ambient = vec4(0.85, 0.85, 0.85, 1.0);
	glUniform4f(light_ambient_ptr, light_ambient[0], light_ambient[1], light_ambient[2], light_ambient[3]);

	light_diffuse = vec4(0.8, 0.8, 0.8, 1.0);
	glUniform4f(light_diffuse_ptr, light_diffuse[0], light_diffuse[1], light_diffuse[2], light_diffuse[3]);

	light_specular = vec4(0.5, 0.5, 0.5, 1.0);
	glUniform4f(light_specular_ptr, light_specular[0], light_specular[1], light_specular[2], light_specular[3]);

	reflect_diffuse = vec4(0.2, 0.2, 0.2, 1.0);
	glUniform4f(reflect_diffuse_ptr, reflect_diffuse[0], reflect_diffuse[1], reflect_diffuse[2], reflect_diffuse[2]);

	reflect_specular = vec4(0.01, 0.01, 0.01, 1.0);
	glUniform4f(reflect_specular_ptr, reflect_specular[0], reflect_specular[1], reflect_specular[2], reflect_specular[3]);
	// Model matrix : an identity matrix (model will be at the origin)

	// Motaz Part----------------------------------------------------
	//	 Set the value of the fragment shader texture sampler variable
	//	   ("texture") to the the appropriate texture unit. In this case,
	//	   zero, for GL_TEXTURE0 which was previously set by calling
	//	   glActiveTexture().
	glUniform1i( glGetUniformLocation(program, "texture"), 0);
	///-------------------------------------------------------

	// glClearColor(1.0, 1.0, 1.0, 1.0); // white background
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	rng = new ring*[_BUFFER_SIZE];

	//fill path with random rings
	for (; II < _BUFFER_SIZE; ++II) {
		generate_next(II);
	}
}

//---------------------------------------------------------------------------

void display(void) {
	// clear the window
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUniformMatrix4fv(m, 1, GL_TRUE, model);
	glUniformMatrix4fv(v, 1, GL_TRUE, view);
	glUniformMatrix4fv(p, 1, GL_TRUE, projection);
	glUniformMatrix4fv(mv, 1, GL_TRUE, move);

	glUniformMatrix4fv(rm, 1, GL_TRUE, translationm);
	for (int i = start; i < _BUFFER_SIZE; ++i) {
		rng[i]->draw();
	}

	// swap buffers since we use double buffer
	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y) {
	switch (key) {
	case 033: // Esc key
		exit(EXIT_SUCCESS);
		break;
	case 040: { // space bar
		// glutPostRedisplay();
		break;
	}
	case 015: // enter key
		break;
	case 's':
		break;
	default:
		break;
	}
}

void reshape(int w, int h) {
	window_width = w;
	window_height = h;
	glViewport(0, 0, (GLsizei) w, (GLsizei) h); // Set our viewport to the size of our window
}

void mouse_click(int button, int state, int x, int y) {
	if (button == 0 && state == 0) {
	}
}

void mouse_motion(int x, int y) {

}

/**
 * method to generate next random part of the path
 */
static int times = 0;
void generate_next(int slot) {
	// cout << "here" << endl;
	int angle = 0;
	if (times > 25 && times < 50)
		angle = 1;
	else if (times > 50 && times < 75)
		angle = 0;
	else if (times > 75 && times < _BUFFER_SIZE)
		angle = 1;
	times++;
	times = times % _BUFFER_SIZE;

	rng[slot] = new ring(0, 0, slot, program, slot % 2 == 0 ? 1.0 : 0.9, slot % 2 == 0 ? 1.0 : 0.9, slot % 2 == 0 ? 1.0 : 0.9);
}

void run(int d) {
	// assign to that slot new ring
//	generate_next(start);

//	// jump to next ring
//	start = (start + 1) % _BUFFER_SIZE;
//
//	// decrement all
//	for (int i = start; i < _BUFFER_SIZE; ++i) {
//		rng[i]->dec_place();
//	}

	if (!stop) {
		translationm = translationm * Translate(0, 0, -ts);

//		if (abs(translationm[2][3]) >= depth) {
//			translationm[2][3] = 0;
//			start = (start + 1) % _BUFFER_SIZE;
//			cout << "dec here " << endl;
//			// decrement all
//			for (int i = start; i < _BUFFER_SIZE; ++i) {
//				rng[i]->dec_place();
//			}
//		}

		glutPostRedisplay();

		if (check_collision()) {
			cout << "woooooow collided !!" << endl;
			stop = true;
		}

		if (!dont)
			glutTimerFunc(0, run, d);
	} else if (stop && acs <= 1.5 * CUBE_SIDE) {
		translationm = translationm * Translate(0, 0, -ts);

		glutPostRedisplay();

		acs += ts;

		if (!dont)
			glutTimerFunc(0, run, d / 10);
	} else {
		return;
	}
}

/*MAGDY PART*************************************************************/
//calculate Euclidean distance in 3D
float eculidian_dist_3D(vec4 curr_pos) {
	return sqrt(pow(camera.x - curr_pos.x, 2) + pow(camera.y - curr_pos.y, 2) + pow(camera.z - curr_pos.z, 2));
}

//multiply vector by matrix
vec4 vec_mat_MUL(vec4 v, mat4 m) {
	vec4 res;
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			res[i] += v[j] * m[i][j];
		}
	}
	return res;
}

//check if the user collided or not
bool check_collision() {
	for (int i = start; i < _BUFFER_SIZE; ++i) {
		surface ** mySurf = rng[i]->getSur();
		for (int j = 0; j < fan; ++j) {
			if (mySurf[j]->has_cube()) {
				cube c = mySurf[j]->getCube();
				vec4 position = vec4(c.centerX, c.centerY, c.centerZ, 1);
				vec4 curr_pos = vec_mat_MUL(position, (projection * view * move * translationm * mySurf[j]->get_orientation()));
				float dist = eculidian_dist_3D(curr_pos);

				if (round(dist) - (CUBE_SIDE + USER_DIM) <= 0.01) {
					acs = 0;
					return true;
				}
			}
		}
	}
	return false;
}
/************************************************************************/

//glutGetModifiers() == GLUT_ACTIVE_CTRL
void special_keys(int key, int x, int y) {

	switch (key) {
	case GLUT_KEY_UP:
		if (dont)
			run(1);
		break;
	case GLUT_KEY_DOWN:
		break;
	case GLUT_KEY_LEFT:
		if (!stop) {
			move = move * RotateZ(-_MOVE_S);
			if (check_collision()) {
				cout << "woooooow collided !!" << endl;
				stop = true;
			}
		}
		glutPostRedisplay();
		break;
	case GLUT_KEY_RIGHT:
		if (!stop) {
			move = move * RotateZ(_MOVE_S);
			if (check_collision()) {
				cout << "woooooow collided !!" << endl;
				stop = true;
			}
		}
		glutPostRedisplay();
		break;
	}
}

//----------------------------------------------------------------------------

int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(window_width, window_height);

//----------------------------------------
// If you are using freeglut, the next two lines will check if
// the code is truly 3.2. Otherwise, comment them out

// glutInitContextVersion( 3, 2 );
// glutInitContextProfile( GLUT_CORE_PROFILE );
//----------------------------------------

	glutCreateWindow("boost");

	glewInit();

	init();

	glutDisplayFunc(display);
	glutMotionFunc(mouse_motion);
	glutMouseFunc(mouse_click);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(special_keys);
	glutReshapeFunc(reshape);

	if (!dont)
		glutTimerFunc(delay, run, delay);

	glutMainLoop();
	return 0;
}

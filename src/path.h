/*
 * path.h
 *
 *  Created on: Dec 15, 2013
 *      Author: Aamer
 */
#include "include/Angel.h"
#include "cube.h"
#include "const.h"

#ifndef PATH_H_
#define PATH_H_

/**
 * basic surface class & function declarations
 */
// ******************************************************************
class surface {
public:
	static const GLfloat gap = 0.0;
	static const GLfloat dgap = 0.0;
	// height from origin
	static const GLfloat rotx = 1.0;
	static const GLfloat roty = 1.0;

	surface();
	surface(bool, GLfloat anglex, GLfloat angley, GLfloat anglez, int place, int fanno, GLuint program, GLfloat r = 1.0, GLfloat g = 1.0, GLfloat b = 1.0);
	void draw();
	void dec_place();
	cube getCube();
	mat4 get_orientation();
	bool has_cube();

private:
	// this variable is used to know the current ring number,
	int place;

	// shader current program
	GLuint program, ww, hh;

	// to know current surface rotation around p(0,0,0)
	mat4 orientation;

	// local vao
	GLuint vao, rotation, ptype, vertex_buffer;
	GLfloat width;
	GLfloat ax, ay;
	mat4 m1, m2;
	// my points
	GLfloat _points[4 * (3 + 3 + 16)];

	//cube on surface
	cube mycube;

	bool bcube;
};
// end of surface functions
// ******************************************************************

/**
 * ring class uses surface class to generate rings, of 12 rotated surface
 * forming a fan, this fan is used later to generate path, note it draws
 * the surfaces in clockwise, since -ve z points out from screen, +ve angles
 * around z leads to -ve clockwise direction
 */
// ******************************************************************
class ring {
public:
	ring();
	ring(GLfloat anglex, GLfloat angley, int place, GLuint program, GLfloat r = 1.0, GLfloat g = 1.0, GLfloat b = 1.0);
	void draw();
	void dec_place();
	surface ** getSur();

private:
	// group of surfaces forming the ring
	surface **surfcs;

	// this variable is used to know the current ring number,
	int place;
};
// end of ring functions
// ******************************************************************

#endif /* PATH_H_ */

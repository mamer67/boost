/*
 * cube.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: AHMAD
 */

#include "cube.h"
using namespace std;

GLuint indices[] = { 0, 1, 3, 2, 4, 5, 7, 6, 6, 5, 2, 1, 7, 6, 3, 2, 7, 4, 3, 0, 4, 5, 0, 1 };
GLuint color_loc;
const vec3 red = vec3(1.0, 0.0, 0.0);
const vec3 green = vec3(0, 0.5, 0);
const vec3 blue = vec3(0.0, 0.0, 1.0);
const vec3 orange = vec3(1.0, 0.3, 0);
const vec3 white = vec3(1.0, 1.0, 1.0);
const vec3 yellow = vec3(1.0, 1.0, 0.0);

cube::cube() {

}

void cube::createCube(GLuint program, GLfloat centerx, GLfloat centery, GLfloat centerz, GLint r, GLint c, GLint n, mat4 m1, mat4 m2) {
	create_cube(centerx, centery, centerz, m1, m2);

	centerX = centerx;
	centerY = centery;
	centerZ = centerz;
	mynumber = n;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(2, &vboID[0]);
	glBindBuffer(GL_ARRAY_BUFFER, vboID[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);
	GLuint loc = glGetAttribLocation(program, "vPosition");
	glEnableVertexAttribArray(loc);
	glVertexAttribPointer(loc, 4, GL_FLOAT, GL_FALSE, 20 * sizeof(GLfloat), BUFFER_OFFSET(0));

	GLuint pos = glGetAttribLocation(program, "xy_s_Matrix");
	// bind the transformation matrix
	GLuint pos1 = pos + 0;
	GLuint pos2 = pos + 1;
	GLuint pos3 = pos + 2;
	GLuint pos4 = pos + 3;
	glEnableVertexAttribArray(pos1);
	glEnableVertexAttribArray(pos2);
	glEnableVertexAttribArray(pos3);
	glEnableVertexAttribArray(pos4);

	glVertexAttribPointer(pos1, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 20, (void*) (sizeof(float) * 4));
	glVertexAttribPointer(pos2, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 20, (void*) (sizeof(float) * 8));
	glVertexAttribPointer(pos3, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 20, (void*) (sizeof(float) * 12));
	glVertexAttribPointer(pos4, 4, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 20, (void*) (sizeof(float) * 16));


	glGenBuffers(1, &ibo[0]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo[0]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices,
	GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, vboID[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);
	color_loc = glGetAttribLocation(program, "vertexColor");
	glEnableVertexAttribArray(color_loc);
	glVertexAttribPointer(color_loc, 3, GL_FLOAT, GL_FALSE, 0,
	BUFFER_OFFSET(0));
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	row = r;
	column = c;

	mat4 newm;
	transformation = newm;
	trans_loc = glGetUniformLocation(program, "cube_rotate");
}

void cube::create_cube(GLfloat centerx, GLfloat centery, GLfloat centerz, mat4 m1, mat4 m2) {
	int i = 0;
	int j = 0;
	points[i++] = vec4(centerx - CUBE_SIDE / 2, centery - CUBE_SIDE / 2, centerz - CUBE_SIDE / 2, 1);
	points[i++] = m1[0];
	points[i++] = m1[1];
	points[i++] = m1[2];
	points[i++] = m1[3];
	colors[j] = vec3(1.0, 1.0, 0.0);

	points[i++] = vec4(centerx + CUBE_SIDE / 2, centery - CUBE_SIDE / 2, centerz - CUBE_SIDE / 2, 1);
	points[i++] = m1[0];
	points[i++] = m1[1];
	points[i++] = m1[2];
	points[i++] = m1[3];
	colors[j++] = vec3(1.0, 1.0, 0.0);

	points[i++] = vec4(centerx + CUBE_SIDE / 2, centery - CUBE_SIDE / 2, centerz + CUBE_SIDE / 2, 1);
	points[i++] = m2[0];
	points[i++] = m2[1];
	points[i++] = m2[2];
	points[i++] = m2[3];
	colors[j++] = vec3(1.0, 1.0, 0.0);

	points[i++] = vec4(centerx - CUBE_SIDE / 2, centery - CUBE_SIDE / 2, centerz + CUBE_SIDE / 2, 1);
	points[i++] = m2[0];
	points[i++] = m2[1];
	points[i++] = m2[2];
	points[i++] = m2[3];
	colors[j++] = vec3(1.0, 1.0, 0.0);

	points[i++] = vec4(centerx - CUBE_SIDE / 2, centery + CUBE_SIDE / 2, centerz - CUBE_SIDE / 2, 1);
	points[i++] = m1[0];
	points[i++] = m1[1];
	points[i++] = m1[2];
	points[i++] = m1[3];
	colors[j++] = vec3(1.0, 1.0, 0.0);

	points[i++] = vec4(centerx + CUBE_SIDE / 2, centery + CUBE_SIDE / 2, centerz - CUBE_SIDE / 2, 1);
	points[i++] = m1[0];
	points[i++] = m1[1];
	points[i++] = m1[2];
	points[i++] = m1[3];
	colors[j++] = vec3(1.0, 1.0, 0.0);

	points[i++] = vec4(centerx + CUBE_SIDE / 2, centery + CUBE_SIDE / 2, centerz + CUBE_SIDE / 2, 1);
	points[i++] = m2[0];
	points[i++] = m2[1];
	points[i++] = m2[2];
	points[i++] = m2[3];
	colors[j++] = vec3(1.0, 1.0, 0.0);

	points[i++] = vec4(centerx - CUBE_SIDE / 2, centery + CUBE_SIDE / 2, centerz + CUBE_SIDE / 2, 1);
	points[i++] = m2[0];
	points[i++] = m2[1];
	points[i++] = m2[2];
	points[i++] = m2[3];
	colors[j++] = vec3(1.0, 1.0, 0.0);

}
vec3 getColor(int i) {
	switch (i) {
	case 0:
		return green;
	case 1:
		return blue;
	case 2:
		return yellow;
	case 3:
		return red;
	case 4:
		return white;
	case 5:
		return orange;
	}
	return yellow;
}

void cube::draw_cube() {
	glBindVertexArray(vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo[0]);
	//glUniformMatrix4fv(trans_loc, 1, GL_TRUE, transformation);
	for (int i = 0; i < 6; i++) {
		glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_INT,
		BUFFER_OFFSET(i * 4 * sizeof(GLuint)));
	}
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

cube::~cube() {
// TODO Auto-generated destructor stub
}


# version 130


// Motaz part
in  vec2 texCoord;
uniform sampler2D texture;

in vec3 fragmentColor;

out vec4 outColor;


// color shading and lighting components
uniform vec4 light_ambient , light_diffuse , light_specular;

uniform vec4 eye;

uniform vec4 reflect_diffuse, reflect_specular;

uniform float shininess;
uniform vec4 light_source_pos;

in vec4 n;
in mat4 transformation;
in vec4 position;

void
main()
{	
	// ambient component
	vec4 ambient_product = light_ambient * texture2D( texture, texCoord);
	
	//vec4 distance = (light_source_pos - position) * (light_source_pos - position);
 
 	//float len = length(distance);
 
 	//if (len <= 0.0)
  		//len = 1.0;
  
	// compute diffuse component
	float d = dot(n, normalize(transformation * light_source_pos));
	vec4 diffuse;
	
	if (d > 0.0)
		diffuse = (light_diffuse * reflect_diffuse) * d;//(len*len);
	else
		diffuse = vec4(0.0 , 0.0, 0.0, 1.0);
	
	// compute specular
	vec4 v = position - eye;
	vec4 h = normalize(light_source_pos + v);
	
	vec4 specular;
	float s = dot(h, n);

	if (s > 0.0)
		specular = (light_specular * reflect_specular) * pow(s, shininess);
	else
		specular = vec4(0.0, 0.0, 0.0, 1.0);
		
	outColor = ambient_product + specular + diffuse;
	
	
	//outColor = texture2D( texture, texCoord);
    //outColor = vec4(fragmentColor, 1);
}